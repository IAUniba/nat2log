% reconsult('/Users/mac/Dropbox/maddy vito/prolog/daPazzi/subordinata.pl').

%:- reconsult('/home/tilly/git/iajavaprolog/res/prolog/commons.pl').
%:- reconsult('/home/madeleinecordasco/repos/IAJavaProlog/res/prolog/commons.pl').
%:- reconsult('/Users/mac/Dropbox/maddy vito/prolog/daPazzi/commons.pl').
:- reconsult('commons.pl').

:- use_module(library(lists)).
:- use_module(library(charsio)).

%ELIMINARE LA S IN FRASE SUBORDINATA MINIMA...NON SERVE

%frasi con il to... I hate eating pasta (xcomp(hate, eating), dobj(eating, pasta)) I hate to eat pasta (aux(eat, to), xcomp(hate, eat), dobj(eating, pasta))
%frasi composte... cc(primoverbo, and/but), conj(verbo1, verbo2) QUELLE CON LA VIRGOLA VENGONO PARSERIZZATE MALE
%frasi con who/which/that nsubj(eats-6, who/that/which), rcmod(man-4, eats-6)
%frasi subordinate avverbiali... mark(verbo1, until/because) advcl(verbo2, verbo1)



fraseSubordinataSemplice(S, Soggetto, Verbo, Pnom, Complementi) :- fraseSubordinataMinima(S1, Sogg, Ve, Pnom), var(Sogg), trova_complementi(Ve, Complementi), length(Complementi, N), N\=0,   verbo_altro(Ve, [V|T]), H=[V, Pnom | Complementi], S2=..H, costruisci_verbo(S2, T, S).
fraseSubordinataSemplice(S, Soggetto, Verbo, Pnom, Complementi) :- fraseSubordinataMinima(S1, Sogg, Ve, Pnom), var(Sogg), trova_complementi(Ve, Complementi), length(Complementi, N), N\=0, H=[Ve, Pnom | Complementi], S=..H.
fraseSubordinataSemplice(S, Soggetto, Verbo, Pnom, Complementi) :- fraseSubordinataMinima(S1, Sogg, Ve, Pnom), trova_complementi(Ve, Complementi), length(Complementi, N), N\=0, verbo_altro(Ve, [V|T]), H=[V, Sogg, Pnom | Complementi], S2=..H,  costruisci_verbo(S2, T, S).
fraseSubordinataSemplice(S, Soggetto, Verbo, Pnom, Complementi) :- fraseSubordinataMinima(S1, Sogg, Ve, Pnom), trova_complementi(Ve, Complementi), length(Complementi, N), N\=0, H=[Ve, Sogg, Pnom | Complementi], S=..H .
fraseSubordinataSemplice(S, Soggetto, Verbo, Complementi) :- fraseSubordinataMinima(S1, Sogg, Ve), var(Sogg), trova_complementi(Ve, Complementi), length(Complementi, N), N\=0,  verbo_altro(Ve, [V|T]), H=[V | Complementi],  S2=..H, costruisci_verbo(S2, T, S).
fraseSubordinataSemplice(S, Soggetto, Verbo, Complementi) :- fraseSubordinataMinima(S1, Sogg, Ve), var(Sogg), trova_complementi(Ve, Complementi), length(Complementi, N), N\=0, H=[Ve | Complementi],  S=..H.
fraseSubordinataSemplice(S, Soggetto, Verbo, Complementi) :- fraseSubordinataMinima(S1, Sogg, Ve), trova_complementi(Ve, Complementi), length(Complementi, N), N\=0,  verbo_altro(Ve, [V|T]), H=[V, Sogg | Complementi],  S2=..H, costruisci_verbo(S2, T, S).
fraseSubordinataSemplice(S, Soggetto, Verbo, Complementi) :- fraseSubordinataMinima(S1, Sogg, Ve), trova_complementi(Ve, Complementi), length(Complementi, N), N\=0, H=[Ve, Sogg | Complementi],  S=..H.



%caso in cui ci sia altro sia per soggetto che per verbo (o anche predicato nominale).
fraseSubordinataMinima(S, Soggetto, Copula, PnomFin) :- fraseSubordinataMinima1(S1, Soggetto, Copula, Pnom), var(Soggetto), verbo_altro(Copula, [V|T]), trova_pnom(PnomFin, Pnom), A=..[V, PnomFin], costruisci_verbo(A, T, S), !.
fraseSubordinataMinima(S, Soggetto, Copula, PnomFin) :- fraseSubordinataMinima1(S1, Soggetto, Copula, Pnom), var(Soggetto), trova_pnom(PnomFin, Pnom), A=..[Copula, PnomFin],  !.
fraseSubordinataMinima(S, Soggetto, Copula, Pnom) :- fraseSubordinataMinima1(S1, Soggetto, Copula, Pnom), var(Soggetto), verbo_altro(Copula, [V|T]), A=..[V, Pnom], costruisci_verbo(A, T, S), !.
fraseSubordinataMinima(S, Soggetto, Copula, Pnom) :- fraseSubordinataMinima1(S, Soggetto, Copula, Pnom), var(Soggetto), S=..[Copula, Pnom], !.

fraseSubordinataMinima(S, SoggettoFin, Copula, PnomFin) :- fraseSubordinataMinima1(S1, Soggetto, Copula, Pnom), trova_soggetti(SoggettoFin, Soggetto), verbo_altro(Copula, [V|T]), trova_pnom(PnomFin, Pnom), A=..[V, SoggettoFin, PnomFin], costruisci_verbo(A, T, S), !.
fraseSubordinataMinima(S, SoggettoFin, Copula, PnomFin) :- fraseSubordinataMinima1(S1, Soggetto, Copula, Pnom), trova_soggetti(SoggettoFin, Soggetto), trova_pnom(PnomFin, Pnom), A=..[Copula, SoggettoFin, PnomFin],  !.
fraseSubordinataMinima(S, Soggetto, Copula, PnomFin) :- fraseSubordinataMinima1(S1, Soggetto, Copula, Pnom), verbo_altro(Copula, [V|T]), trova_pnom(PnomFin, Pnom), A=..[V, Soggetto, PnomFin], costruisci_verbo(A, T, S), !.
fraseSubordinataMinima(S, Soggetto, Copula, PnomFin) :- fraseSubordinataMinima1(S1, Soggetto, Copula, Pnom), trova_pnom(PnomFin, Pnom), A=..[Copula, Soggetto, PnomFin],  !.
fraseSubordinataMinima(S, SoggettoFin, Copula, Pnom) :- fraseSubordinataMinima1(S1, Soggetto, Copula, Pnom), trova_soggetti(SoggettoFin, Soggetto), verbo_altro(Copula, [V|T]), A=..[V, SoggettoFin, Pnom], costruisci_verbo(A, T, S), !.
fraseSubordinataMinima(S, SoggettoFin, Copula, Pnom) :- fraseSubordinataMinima1(S1, Soggetto, Copula, Pnom), trova_soggetti(SoggettoFin, Soggetto), A=..[Copula, SoggettoFin, Pnom],  !.
fraseSubordinataMinima(S, Soggetto, Copula, Pnom) :- fraseSubordinataMinima1(S1, Soggetto, Copula, Pnom), verbo_altro(Copula, [V|T]), A=..[V, Soggetto, Pnom], costruisci_verbo(A, T, S), !.
fraseSubordinataMinima(S, Soggetto, Copula, Pnom) :- fraseSubordinataMinima1(S, Soggetto, Copula, Pnom), S=..[Copula, Soggetto, Pnom], !.


fraseSubordinataMinima(S, Soggetto, Verbo) :- fraseSubordinataMinima2(S1, Soggetto, Verbo), var(Soggetto), verbo_altro(Verbo, [V|T]), A=V, costruisci_verbo(A, T, S), !.
fraseSubordinataMinima(S, Soggetto, Verbo) :- fraseSubordinataMinima2(S, Soggetto, Verbo), var(Soggetto), S=Verbo, !.

fraseSubordinataMinima(S, SoggettoFin, Verbo) :- fraseSubordinataMinima2(S1, Soggetto, Verbo), trova_soggetti(SoggettoFin, Soggetto), verbo_altro(Verbo, [V|T]), A=..[V, SoggettoFin], costruisci_verbo(A, T, S), !.
fraseSubordinataMinima(S, Soggetto, Verbo) :- fraseSubordinataMinima2(S1, Soggetto, Verbo), verbo_altro(Verbo, [V|T]), A=..[V, Soggetto], costruisci_verbo(A, T, S), !.
fraseSubordinataMinima(S, SoggettoFin, Verbo) :- fraseSubordinataMinima2(S1, Soggetto, Verbo), trova_soggetti(SoggettoFin, Soggetto), S=..[Verbo, SoggettoFin], !.
fraseSubordinataMinima(S, Soggetto, Verbo) :- fraseSubordinataMinima2(S, Soggetto, Verbo), S=..[Verbo, Soggetto], !.



%caso base 
fraseSubordinataMinima1(S, Soggetto, Copula, Pnom) :- soggetto_verboSub(Pnom, Soggetto), predNom_verboSub(Pnom, Copula), S=..[Copula, Soggetto, Pnom], !.
fraseSubordinataMinima2(S, Soggetto, Verbo) :-  soggetto_verboSub(Verbo, Soggetto), not(cop2(Verbo, Copula)), S=..[Verbo, Soggetto], !.




%soggetto_verboSub(Verbo, Soggetto) :- nsubj(Verbo, Soggetto), !.
%soggetto_verboSub(Verbo, Soggetto) :- nsubjpass(Verbo, Soggetto), !.
soggetto_verboSub(Verbo, Soggetto) :- rcmod(Soggetto, Verbo), !.
soggetto_verboSub(Verbo, Soggetto) :- trova_verboSub(Verbo), trova_soggettoSub(Soggetto, Verbo), !.



%per le frasi subordinate con copula, al posto di Verbo1 ci sarà il predicato nominale
predNom_verboSub(PredNominale, Copula) :- trova_verboSub(PredNominale), cop2(PredNominale, Copula).



trova_verboSub(Verbo2) :- xcomp(Verbo1, Verbo2).
trova_verboSub(Verbo2) :- advcl(Verbo1, Verbo2).
trova_verboSub(Verbo2) :- ccomp(Verbo1, Verbo2).
trova_verboSub(Verbo2) :- vmod(Verbo1, Verbo2).
trova_verboSub(Verbo2) :- conj(Verbo1, Verbo2).

%POTREBBE NON AVERE SOGGETTO i like eating/ i like to eat
trova_soggettoSub(Soggetto, Verbo) :- nsubj(Verbo, Soggetto).
trova_soggettoSub(Soggetto, Verbo) :- nsubjpass(Verbo, Soggetto).
%quando arriva a trovasoggettosub ha già verificato la presenza di conj, xcomp e advcl, quindi se non trova soggetti è xkè effettivamente nn ce ne sono
trova_soggettoSub(Soggetto, Verbo).






/* This is the shop where I bought my bike.
nsubj(shop-4, This-1)
cop(shop-4, is-2)
det(shop-4, the-3)
root(ROOT-0, shop-4)
advmod(bought-7, where-5)
nsubj(bought-7, I-6)
rcmod(shop-4, bought-7)
poss(bike-9, my-8)
dobj(bought-7, bike-9)
*/

/* seaman is someone who works on a ship.
det(seaman-2, A-1)
nsubj(someone-4, seaman-2)
cop(someone-4, is-3)
root(ROOT-0, someone-4)
nsubj(works-6, who-5)
rcmod(someone-4, works-6)
prep(works-6, on-7)
det(ship-9, a-8)
pobj(on-7, ship-9)
*/
