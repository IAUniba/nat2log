package utils.models;

public class ProperNoun {

	String noun;
	String pos;
	String ne;

	public ProperNoun(String noun, String pos, String ne) {
		super();
		this.noun = noun;
		this.pos = pos;
		this.ne = ne;
	}

	public String getNoun() {
		return noun;
	}

	public void setNoun(String noun) {
		this.noun = noun;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getNe() {
		return ne;
	}

	public void setNe(String ne) {
		this.ne = ne;
	}

	@Override
	public String toString() {
		return "ProperNoun [noun=" + noun + ", pos=" + pos + ", ne=" + ne + "]";
	}
	
	
	
}
