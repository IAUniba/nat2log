package utils;


import java.io.File;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;


import utils.models.BaseForm;
import utils.models.ProperNoun;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.util.CoreMap;

public class InthelexUtils {
	
	public static String[] getSentenceInthelex(List<HasWord> sentence) {
		List<String> sent = new LinkedList<>();
		for (HasWord w : sentence) {
			sent.add(w.toString());
		}

		String[] sentArr = new String[sent.size()];
		sentArr = sent.toArray(sentArr);
		return sentArr;
	}
	
	
	public static List<String> relationPreprocessingInthelex(
			Collection<TypedDependency> ti) {

		List<String> relations = new LinkedList<>();
		List<String> newRelations = new LinkedList<>();
		// int i=0;
		for (TypedDependency t : ti) {
			String relation = t.toString().toLowerCase();

			
			
			String[] spl = relation.split("-");

			relation = spl[0] + ", '" + spl[1].split(",")[0] + "', "
					+ spl[1].split(",")[1] + ", '" + spl[2].split("[)]")[0]
					+ "')";
			relation = relation.replaceAll("[.]", "");
			if (relation.contains("'s")) {
				continue;
			}
			
			relation =relation.replaceAll("[-]",  "");
			relation = relation.toLowerCase();
			relation = Utils.getDefaultPred(relation);
			relation = Utils.getNot(relation);

			newRelations.add(relation);

		}
		return newRelations;
	}
	
	
	// aggiunge ed elimina predicati in caso di futuro passato o ing form
		public static List<String> setBaseFormInthelex(List<BaseForm> baseForms,
				List<String> relations) {

			// CONTROLLO SULLE BASE FORM
			if (!baseForms.isEmpty()) {
				for (BaseForm b : baseForms) {
					// System.out.println(b.getBase_form());
					// ogni verbo particolare deve essere sostituito con la sua base
					// form
					for (int i = 0; i < relations.size(); i++) {
						if (relations.get(i).contains(",  " + b.getVerb())
								|| relations.get(i).contains("(" + b.getVerb())) {
							relations.set(
									i,
									relations.get(i).replaceAll(b.getVerb(),
											b.getBase_form()));
						}
					}

				}

			}

			return relations;
		}
		
		
		
		public static String getOtherCategories(File[] listOfFiles, String name,
				String string) {
			List<String> categories = new LinkedList<>();
			String cat = new String();
			for (File f : listOfFiles) {
				if (f.getName().equals(name))
					continue;
				categories.add(f.getName().split("[.]")[0]);
			}

			if (!categories.isEmpty()) {
				StringBuilder categoryRel = new StringBuilder();

				categoryRel.append("all_categories([");
				for (int i = 0; i < categories.size() - 1; i++) {
					categoryRel.append("neg(" + categories.get(i) + "(" + string
							+ ")), ");
				}
				categoryRel.append("neg(" + categories.get(categories.size() - 1)
						+ "(" + string + "))])");

				cat = categoryRel.toString();
			}

			return cat;
		}
		
		
		
		@SuppressWarnings("unchecked")
		public static List<String> getProperNames(String sent,
				List<String> relations, StanfordCoreNLP pipeline) {
			
			List<String> newRelations = new LinkedList<>();
			List<ProperNoun> words = new LinkedList<>();
			
			String text = sent;

			Annotation document = new Annotation(text);

			pipeline.annotate(document);

			// a CoreMap is essentially a Map that uses class objects as keys and
			// has values with custom types
			List<CoreMap> sentences = document.get(SentencesAnnotation.class);

			for (CoreMap sentence : sentences) {
				// traversing the words in the current sentence
				// a CoreLabel is a CoreMap with additional token-specific methods
				for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
					String noun = token.get(TextAnnotation.class);
					String pos = token.get(PartOfSpeechAnnotation.class);
					String ne = token.get(NamedEntityTagAnnotation.class);
					
					ProperNoun pn = new ProperNoun(noun, pos, ne);
					words.add(pn);
				}

			}


			for (ProperNoun w : words) {

				String pos = w.getPos();
				String ne = w.getNe();
				String token = w.getNoun();
				if (pos.equals("NN") || pos.equals("NNP") || pos.equals("NNS")) {

					newRelations.add("noun(" + token.toLowerCase().replaceAll("[.|-]", "")+")");

				}
				if(ne.equals("PERSON")){
					newRelations.add("person(" + token.toLowerCase().replaceAll("[.|-]", "")+")");
				}
				else if(ne.equals("ORGANIZATION")){
					newRelations.add("organization(" + token.toLowerCase().replaceAll("[.|-]", "")+")");
				}

			}
			newRelations.addAll(relations);

			return newRelations;
		}


		public static String getString(String[] sentArray) {
			
			StringBuilder builder = new StringBuilder();
			for(String s:sentArray){
				builder.append(s+" ");
			}
			builder.append(".");
			
			return builder.toString();
			
		}
	
		
		
}
