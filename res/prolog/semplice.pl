% reconsult('/Users/mac/Dropbox/maddy vito/prolog/daPazzi/semplice.pl').

%:- reconsult('/home/tilly/git/iajavaprolog/res/prolog/commons.pl').
%:- reconsult('/home/madeleinecordasco/repos/IAJavaProlog/res/prolog/commons.pl').
%:- reconsult('/Users/mac/Dropbox/maddy vito/prolog/daPazzi/commons.pl').
:- reconsult('commons.pl').

:- use_module(library(lists)).
:- use_module(library(charsio)).

/*
posso sfruttare il trova relazioni ricorsivo su soggetto, e oggetto. questo dovrebbe garantire l'annidamento dei complementi relativi ai soggetti.
per quanto riguarda i complementi relativi al verbo, essi devono avere un posto riservato nella frase, quindi diventerebbe
verbo(soggetto, oggetto, complemento/i) ovviamente il complemento oggetto rientra nei complementi del verbo
i complementi del verbo possono essere multipli, per questo verranno salvati in una lista e collegati successivamente nelle frasi

*/

%Complementi è una lista che contiene i complementi relativi a soggetto, verbo e oggetto
%considerare i complementi relativi ai complementi stessi


fraseSemplice(S, Soggetto, Verbo, Pnom, Complementi) :- fraseMinima(S1, Sogg, Ve, Pnom), trova_complementi(Ve, Complementi), length(Complementi, N), N\=0, verbo_altro(Ve, [V|T]), H=[V, Sogg, Pnom | Complementi], S2=..H, costruisci_verbo(S2, T, S).
fraseSemplice(S, Soggetto, Verbo, Complementi) :- fraseMinima(S1, Sogg, Ve), trova_complementi(Ve, Complementi), length(Complementi, N), N\=0, verbo_altro(Ve, [V|T]), H=[V, Sogg | Complementi], S2=..H, costruisci_verbo(S2, T, S).





