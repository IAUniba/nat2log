Google reinstates 'forgotten' links.
Google bans porn ads.
Plasma TV has almost disappeared.
Sonar rods use ultrasound to guiding blind people.
Google buys radio service.
Bitcoin is a PayPal competitor for retailers.
Google removes the BBC links.
Facebook attacked for emotion study.
Facebook buys video startup.
Mobile apps was transforming the future.
Facebook treats you like a lab rat.
The Internet of Things is still young, but it is real.
There are already dozens of internet-connected devices available.
17 percent of the world’s software developers are already working on Internet of Things projects. 
The world’s largest tech companies are already in fierce competition to attract developers to their respective connected device platforms. 
Google is hoping to expand its strength in smart phones to other connected devices.
Samsung is using Android for its Gear smart watches.
The company is also promoting its open source Tizen operating system for wearables and other devices.
