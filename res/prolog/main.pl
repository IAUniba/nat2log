 /*
 :- reconsult('/Users/mac/Dropbox/maddy vito/prolog/daPazzi/minima.pl').
 :- reconsult('/Users/mac/Dropbox/maddy vito/prolog/daPazzi/semplice.pl').
 :- reconsult('/Users/mac/Dropbox/maddy vito/prolog/daPazzi/subordinata.pl').
*/
 
 
% :- reconsult('/home/madeleinecordasco/repos/IAJavaProlog/res/prolog/minima.pl').
% :- reconsult('/home/madeleinecordasco/repos/IAJavaProlog/res/prolog/semplice.pl').
% :- reconsult('/home/madeleinecordasco/repos/IAJavaProlog/res/prolog/subordinata.pl').
%:- reconsult('/home/tilly/git/iajavaprolog/res/prolog/minima.pl').
%:- reconsult('/home/tilly/git/iajavaprolog/res/prolog/semplice.pl').
%:- reconsult('/home/tilly/git/iajavaprolog/res/prolog/subordinata.pl').
:- reconsult('minima.pl').
:- reconsult('semplice.pl').
:- reconsult('subordinata.pl').


% reconsult('/Users/mac/Dropbox/maddy vito/prolog/daPazzi/main.pl').

:- dynamic nsubj/2.
:- dynamic dobj/2.
:- dynamic det/2.
:- dynamic poss/2.
:- dynamic amod/2.
:- dynamic rcmod/2.
:- dynamic advmod/2.
:- dynamic neg/2.
:- dynamic prep/2.
:- dynamic pobj/2.
:- dynamic nn2/2.
:- dynamic prt/2.
:- dynamic cop2/2.
:- dynamic num/2.
:- dynamic aux/2.
:- dynamic nsubjpass/2.
:- dynamic cc/2.
:- dynamic auxpass/2.
:- dynamic type/2.
:- dynamic iobj/2.
:- dynamic quantmod/2.
:- dynamic acomp/2.
:- dynamic nsubj2/2.
:- dynamic nsubjpass2/2.
:- dynamic nn3/2.
:- dynamic dobj2/2.
:- dynamic ccomp/2.
:- dynamic mark/2.
:- dynamic xcomp/2.
:- dynamic vmod/2.
:- dynamic number/2.
:- dynamic conj/2.
:- dynamic advcl/2.





%FARE RETRACTALL!!!


%i was living
%i was living with my friend.
%i hate living with my friend.
%i hate to live with my friend.

/*
sentence(S) :- advcl(X, Y), fraseSub(S), !.
sentence(S) :- xcomp(X, Y), fraseSub(S), !.
sentence(S) :- rcmod(X, Y), fraseSub(S), !.
sentence(S) :- ccomp(X, Y), fraseSub(S), !.
sentence(S) :- nsubj(X, Y), conj(X, Verb), nsubj(Verb, A), fraseSub(S), !.
sentence(S) :- nsubjpass(X, Y), conj(X, Verb), nsubj(Verb, A), fraseSub(S), !.
sentence(S) :- nsubj(X, Y), conj(X, Verb), nsubjpass(Verb, A), fraseSub(S), !.
sentence(S) :- nsubjpass(X, Y), conj(X, Verb), nsubjpass(Verb, A), fraseSub(S), !.


sentence(S) :- fraseSemplice(S, Sogg, Ve, Pnom, Complementi), !.
sentence(S) :- fraseSemplice(S, Sogg, Ve, Complementi), !.
sentence(S) :- fraseMinima(S, Sogg, Ve, Pnom), !.
sentence(S) :- fraseMinima(S, Sogg, Ve), !.
*/

sentence(S) :- advcl(X, Y), fraseSub(H), got_atom(H, S), !.
sentence(S) :- xcomp(X, Y), fraseSub(H), got_atom(H, S), !.
sentence(S) :- rcmod(X, Y), fraseSub(H), got_atom(H, S), !.
sentence(S) :- ccomp(X, Y), fraseSub(H), got_atom(H, S), !.
sentence(S) :- vmod(X, Y), fraseSub(H), got_atom(H, S), !.
sentence(S) :- nsubj(X, Y), conj(X, Verb), nsubj(Verb, A), fraseSub(H), got_atom(H, S), !.
sentence(S) :- nsubjpass(X, Y), conj(X, Verb), nsubj(Verb, A), fraseSub(H), got_atom(H, S), !.
sentence(S) :- nsubj(X, Y), conj(X, Verb), nsubjpass(Verb, A), fraseSub(H), got_atom(H, S), !.
sentence(S) :- nsubjpass(X, Y), conj(X, Verb), nsubjpass(Verb, A), fraseSub(H), got_atom(H, S), !.



sentence(S) :- fraseSemplice(H, Sogg, Ve, Pnom, Complementi), got_atom(H, S), !.
sentence(S) :- fraseSemplice(H, Sogg, Ve, Complementi), got_atom(H, S), !.
sentence(S) :- fraseMinima(H, Sogg, Ve, Pnom), got_atom(H, S), !.
sentence(S) :- fraseMinima(H, Sogg, Ve), got_atom(H, S), !.



fraseSub(S) :- fraseSubordinataSemplice(S2, Sogg, Ve, Pnom, Complementi), fraseSemplice(S1, Sogg1, Ve1, Pnom1, Complementi1), !,   S=..[sub, S1, S2].
fraseSub(S) :- fraseSubordinataSemplice(S2, Sogg, Ve, Pnom, Complementi), fraseSemplice(S1, Sogg1, Ve1, Complementi1), !,  S=..[sub, S1, S2].
fraseSub(S) :- fraseSubordinataSemplice(S2, Sogg, Ve, Pnom, Complementi), fraseMinima(S1, Sogg1, Ve1, Pnom1), !,   S=..[sub, S1, S2].
fraseSub(S) :- fraseSubordinataSemplice(S2, Sogg, Ve, Pnom, Complementi), fraseMinima(S1, Sogg1, Ve1), !,   S=..[sub, S1, S2].

fraseSub(S) :- fraseSubordinataSemplice(S2, Sogg, Ve, Complementi), fraseSemplice(S1, Sogg1, Ve1, Pnom1, Complementi1), !,   S=..[sub, S1, S2].
fraseSub(S) :- fraseSubordinataSemplice(S2, Sogg, Ve, Complementi), fraseSemplice(S1, Sogg1, Ve1, Complementi1), !,  S=..[sub, S1, S2].
fraseSub(S) :- fraseSubordinataSemplice(S2, Sogg, Ve, Complementi), fraseMinima(S1, Sogg1, Ve1, Pnom1), !,  S=..[sub, S1, S2].
fraseSub(S) :- fraseSubordinataSemplice(S2, Sogg, Ve, Complementi), fraseMinima(S1, Sogg1, Ve1),!,  S=..[sub, S1, S2].

fraseSub(S) :- fraseSubordinataMinima(S2, Sogg, Ve, Pnom), fraseSemplice(S1, Sogg1, Ve1, Pnom1, Complementi1), !,  S=..[sub, S1, S2].
fraseSub(S) :- fraseSubordinataMinima(S2, Sogg, Ve, Pnom), fraseSemplice(S1, Sogg1, Ve1, Complementi1), !,  S=..[sub, S1, S2].
fraseSub(S) :- fraseSubordinataMinima(S2, Sogg, Ve, Pnom), fraseMinima(S1, Sogg1, Ve1, Pnom1), !,  S=..[sub, S1, S2].
fraseSub(S) :- fraseSubordinataMinima(S2, Sogg, Ve, Pnom), fraseMinima(S1, Sogg1, Ve1), !, S=..[sub, S1, S2].

fraseSub(S) :- fraseSubordinataMinima(S2, Sogg, Ve), fraseSemplice(S1, Sogg1, Ve1, Pnom1, Complementi1), !,  S=..[sub, S1, S2].
fraseSub(S) :- fraseSubordinataMinima(S2, Sogg, Ve), fraseSemplice(S1, Sogg1, Ve1, Complementi1),!,  S=..[sub, S1, S2].
fraseSub(S) :- fraseSubordinataMinima(S2, Sogg, Ve), fraseMinima(S1, Sogg1, Ve1, Pnom1),!,  S=..[sub, S1, S2].
fraseSub(S) :- fraseSubordinataMinima(S2, Sogg, Ve), fraseMinima(S1, Sogg1, Ve1), !, S=..[sub, S1, S2].









