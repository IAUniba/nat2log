package utils.models;

public enum VerbType {

	PRESENT_PROGRESSIVE, PAST_PROGRESSIVE, FUTURE_PROGRESSIVE, PAST, FUTURE, THIRD, BE, PASSIVE_PAST, PASSIVE
}
