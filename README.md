# Nat2Log #

Nat2Log è una libreria che permette la trasformazione di frasi in linguaggio naturale, in frasi in formalismo logico.

## Requisiti di sistema ##

Nat2Log è funzionante su computer che possiedono:

* Sistema Operativo Linux e distribuzioni derivate

* YAP Prolog v6.2.2

* Java Virtual Machine

I requisiti Hardware sono minimi, all’aumentare della potenza di calcolo disponibile diminuiranno i tempi di elaborazione necessari.

Nat2Log è stato testato su:


Macchina fisica con:

  - OS Ubuntu 14.04

  - Processore Intel i7-2720QM 2,2GHz 

  - Ram 8GB

  - Hard Disk da 500GB 7200rpm

Macchina virtuale con:

  - OS Ubuntu 14.04

  - Processore Intel i5 2,5GHz,1core 

  - Ram 1GB

  - Hard Disk da 20GB 5400rpm

### Librerie non incluse nel progetto ###

Others libraries are available in Download folder.
Those libraries must be copied in "lib" folder.

