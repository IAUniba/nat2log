import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.annolab.tt4j.TreeTaggerException;
import org.annolab.tt4j.TreeTaggerWrapper;

import utils.InthelexUtils;
import utils.Utils;
import utils.Verbs;
import utils.models.BaseForm;

import com.declarativa.interprolog.PrologEngine;
import com.declarativa.interprolog.YAPSubprocessEngine;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;

public class InthelexPre {

	GrammaticalStructureFactory gsf;
	PrologEngine engine;
	LexicalizedParser lp;
	TreeTaggerWrapper tagger;
	StanfordCoreNLP pipeline;

	public InthelexPre() throws IOException {

		// parser
		lp = LexicalizedParser
				.loadModel("edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz");
		TreebankLanguagePack tlp = new PennTreebankLanguagePack();
		gsf = tlp.grammaticalStructureFactory();

		// treetagger
		System.setProperty("treetagger.home", "res/treeTagger");
		tagger = new TreeTaggerWrapper<String>();
		tagger.setModel("res/treeTagger/models/english-utf8.par");

		// stanford ner
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma, ner");
		pipeline = new StanfordCoreNLP(props);

		// prolog
		engine = new YAPSubprocessEngine("/usr/bin/yap", false);
		File f = new File("./res/prolog/inthelex.pl");
		engine.consultAbsolute(f);

	}

	public List<String> preProcess(String directoryPath) throws IOException,
			TreeTaggerException {

		File folder = new File(directoryPath);
		File[] listOfFiles = folder.listFiles();
		List<String> fin = new LinkedList<>();

		for (File f : listOfFiles) {

			String path = directoryPath + "/" + f.getName();
			String categoria = f.getName().split("[.]")[0];
			String cat = categoria.substring(0, 3).toLowerCase();
			
			int i = 0;
			
			// apre il file count e legge la i
			File count = new File("./res/count" + categoria + ".txt");
			if (!count.exists()) {
				count.createNewFile();
			} else {

				FileReader fr = new FileReader(count.getAbsoluteFile());
				BufferedReader br = new BufferedReader(fr);

				i = Integer.parseInt(br.readLine());

				br.close();

			}
			
			for (List<HasWord> sentence : new DocumentPreprocessor(path)) {


				System.out.println("FRASE " + ++i);
				String[] sentArray = InthelexUtils
						.getSentenceInthelex(sentence);
				String sent = InthelexUtils.getString(sentArray);

				List<BaseForm> baseForms = Verbs.getBaseForm(sentArray, tagger);

				Tree parse = lp.apply(sentence);

				// ottiene le dipendenze dal parser
				GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
				Collection<TypedDependency> ti = gs.allTypedDependencies();

				List<String> relations = InthelexUtils
						.relationPreprocessingInthelex(ti);
				List<String> newRelations = InthelexUtils.setBaseFormInthelex(
						baseForms, relations);
				newRelations = InthelexUtils.getProperNames(sent, newRelations,
						pipeline);

				newRelations.add("categoria(" + categoria + ")");
				newRelations.add("frase(frase" + cat + i + ")");
				newRelations.add("name_cat(" + cat + i + ")");

				String altreCategorie = InthelexUtils.getOtherCategories(
						listOfFiles, f.getName(), "frase" + cat + i);
				if (!altreCategorie.isEmpty()) {
					newRelations.add(altreCategorie);
				} else {
					newRelations.add("all_categories([])");
				}

				for (String s : newRelations) {
					System.out.println(s);
				}
				System.out.println();

				for (String s : newRelations) {
					engine.deterministicGoal("assert(" + s + ")");
				}

				Object[] bindings = engine.deterministicGoal("getFrase(F)",
						"[string(F)]");
				if (bindings.length == 0) {
					continue;
				}
				String message = (String) bindings[0];
				fin.add(message);

				// Retract
				for (String s : newRelations) {
					engine.deterministicGoal("retract(" + s + ")");
				}
				engine.deterministicGoal("retractall(map(X, Y))");
				// System.out.println("\nSentence:" + message + "\n");

				// register new i
				FileWriter fw = new FileWriter(count.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				Integer in = (Integer) i;
				bw.write(in.toString());

				bw.close();

			}

			fin.add("");

		}
		engine.shutdown();
		return fin;
	}

}
