package utils;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.trees.TypedDependency;

/*
 * fornisce metodi per la gestione e la modifica delle dipendenze
 * */
public class Utils {

	// converte la list di haswords in array di stringhe
	public static String[] getSentence(List<HasWord> sentence) {
		List<String> sent = new LinkedList<>();
		for (HasWord w : sentence) {
			sent.add(w.toString().toLowerCase());
		}

		String[] sentArr = new String[sent.size()];
		sentArr = sent.toArray(sentArr);
		return sentArr;
	}

	// modifica opportunamente le relazioni
	public static List<String> relationPreprocessing(
			Collection<TypedDependency> ti) {

		List<String> relations = new LinkedList<>();
		for (TypedDependency t : ti) {
			String relation = t.toString().toLowerCase();

			relation = relation.replaceAll("-[0-9]+", "");

			relation = relation.replaceAll("[.]", "");
			if (relation.contains("'s")) {
				continue;
			}

			relation = relation.replaceAll("[-]", "");

			relation = relation.toLowerCase();
			relation = getDefaultPred(relation);
			relation = getNot(relation);

			if (relation
					.matches("[a-z|A-Z|0-9]+[(][a-z|A-Z]+, [0-9]+[a-zA-Z]*[)]")) {
				relation = modifyNumbers(relation);
			}

			if (relation
					.matches("[a-z|A-Z|0-9]+[(][a-z|A-Z]+, [a-zA-Z]+'[a-zA-Z]+[)]")) {
				relation = relation.replaceAll("'", "");
			}
			relation = relation.replaceAll("-", "");
			relation = relation.replaceAll(":", "");
			relation = relation.replaceAll(";", "");

			relations.add(relation);

		}
		return relations;
	}

	private static String modifyNumbers(String relation) {

		String spl = relation.split(",")[1].split("\\)")[0];
		if (spl.matches("[0-9]+[a-zA-Z]+"))
			return relation.split(",")[0] + ", '" + spl.trim() + "')";

		String fin = relation.split(",")[0] + ", '" + spl.trim() + "')";
		return fin;
	}

	/*
	 * sostituisce i predicati prolog ambigui
	 */
	static String getDefaultPred(String relation) {

		String[] splt = relation.split("[(]");

		switch (splt[0]) {
		case "nn":
			String pred = "nn2(" + splt[1];
			return pred;
		case "cop":
			String pred2 = "cop2(" + splt[1];
			return pred2;
		default:
			return relation;
		}

	}

	static String getNot(String relation) {

		if (relation.contains("neg")) {
			String[] spl = relation.split(",");
			String out = spl[0] + (", no)");
			return out;
		}

		return relation;
	}

}
