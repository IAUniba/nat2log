%reconsult('/Users/mac/Dropbox/maddy vito/prolog/inthelex/inthelex.pl').
%reconsult('/home/madeleinecordasco/repos/IAJavaProlog/res/prolog/inthelex.pl').

:- use_module(library(lists)).
:- use_module(library(charsio)).

:- dynamic nsubj/4.
:- dynamic dobj/4.
:- dynamic det/4.
:- dynamic poss/4.
:- dynamic amod/4.
:- dynamic rcmod/4.
:- dynamic advmod/4.
:- dynamic neg/4.
:- dynamic prep/4.
:- dynamic pobj/4.
:- dynamic nn2/4.
:- dynamic prt/4.
:- dynamic cop2/4.
:- dynamic num/4.
:- dynamic aux/4.
:- dynamic auxpass/4.
:- dynamic nsubjpass/4.
:- dynamic cc/4.
:- dynamic conj/4.
:- dynamic number/4.
:- dynamic quantmod/4.


%java, frase completa, aggiungere altri esempi di frasi subordinate


%getFrase(F) :- getRule(Rule), getSentence(Rule, F1).
getFrase(F) :- getRule(Rule), getSentence(Rule, F1), got_atom(F1, F).



getSentence([H|T], F) :-  getSentenceRic(T, F2, Provv), get_testa(Testa, H), got_atom(Testa, A), atom_concat(A, ' :- ', R), atom_concat(R, F2, F1), !, got_atom(F, F1). 
getSentenceRic([], Provv, Provv).
getSentenceRic([H|T], F, Provv) :- var(Provv), got_atom(H, A), getSentenceRic(T, F, A).
getSentenceRic([H|T], F, Provv) :- got_atom(H, A), atom_concat(A, ',', R), atom_concat(R, Provv, Provv2), getSentenceRic(T, F, Provv2).

get_testa(Testa, H) :- all_categories(Lista), Lista==[], Testa=H, !.
get_testa(Testa, H) :- all_categories(Lista), list_concat([[H], Lista], All), get_testaRic(All, Testa, Provv).
get_testaRic([], Testa, Provv) :- atom_concat('[', Provv, A), atom_concat(A, ']', Testa).
get_testaRic([H|T], Testa, Provv):- var(Provv), got_atom(H, A), get_testaRic(T, Testa, A).
get_testaRic([H|T], Testa, Provv):- got_atom(H, A), atom_concat(A, ', ', A1), atom_concat(A1, Provv, Provv2), get_testaRic(T, Testa, Provv2).
  

getRule(Rule) :- categoria(Cat), frase(N), name_cat(Ca), F=..[Cat, N], getRulesRic(Provv, Rule2), Rule=[F|Rule2], !.


%NSUBJ
getRulesRic(Provv, Rule) :- nsubj(Verbo, N1, Soggetto, N2),  frase(N), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N1, C1), atom_concat(Z, N2, C2),  R1=..[nsubj, C1, C2], R2=..[verbo, C1, C2],  R4=..[frase, N, C1], Provv2=[R1, R2, R4| Provv], assert(map(Verbo, C1)), assert(map(Soggetto, C2)), retract(nsubj(Verbo, N1, Soggetto, N2)), getRulesRic(Provv2, Rule).
%getRulesRic(Provv, Rule) :- nsubj(Verbo, N1, Soggetto, N2), frase(N), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N1, C1), atom_concat(Z, N2, C2),  R1=..[nsubj, C1, C2], R2=..[tax, C1, [Verbo]], R3=..[tax, C2, [Soggetto]], R4=..[frase, N, C1], Provv2=[R1, R2, R3, R4| Provv], assert(map(Verbo, C1)), assert(map(Soggetto, C2)), retract(nsubj(Verbo, N1, Soggetto, N2)), getRulesRic(Provv2, Rule).

%NSUBJPASS
getRulesRic(Provv, Rule) :- nsubjpass(Verbo, N1, Soggetto, N2),  frase(N), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N1, C1), atom_concat(Z, N2, C2),  R1=..[nsubj, C1, C2],  R2=..[verbo, C1, C2], R4=..[frase, N, C1], NNP=..[nnp, C2], Provv2=[R1, R2, R4| Provv], assert(map(Verbo, C1)), assert(map(Soggetto, C2)), retract(nsubjpass(Verbo, N1, Soggetto, N2)), getRulesRic(Provv2, Rule).
%getRulesRic(Provv, Rule) :- nsubjpass(Verbo, N1, Soggetto, N2), frase(N), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N1, C1), atom_concat(Z, N2, C2),  R1=..[nsubj, C1, C2],  R2=..[tax, C1, [Verbo]], R3=..[tax, C2, [Soggetto]], R4=..[frase, N, C1], Provv2=[R1, R2, R3, R4| Provv], assert(map(Verbo, C1)), assert(map(Soggetto, C2)), retract(nsubjpass(Verbo, N1, Soggetto, N2)), getRulesRic(Provv2, Rule).

%RCMOD
getRulesRic(Provv, Rule) :- rcmod(Soggetto, N1, Verbo, N2),  frase(N), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N1, C1), atom_concat(Z, N2, C2),  R1=..[nsubj, C1, C2], R2=..[verbo, C2, C1], R4=..[frase, N, C2], NNP=..[nnp, C1], Provv2=[R1, R2, R4| Provv], assert(map(Verbo, C2)), assert(map(Soggetto, C1)), retract(rcmod(Soggetto, N1, Verbo, N2)), getRulesRic(Provv2, Rule).
%getRulesRic(Provv, Rule) :- rcmod(Soggetto, N1, Verbo, N2), frase(N), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N1, C1), atom_concat(Z, N2, C2),  R1=..[nsubj, C1, C2], R2=..[tax, C2, [Verbo]], R3=..[tax, C1, [Soggetto]], R4=..[frase, N, C2], Provv2=[R1, R2, R3, R4| Provv], assert(map(Verbo, C2)), assert(map(Soggetto, C1)), retract(rcmod(Soggetto, N1, Verbo, N2)), getRulesRic(Provv2, Rule).



%AUX
%getRulesRic(Provv, Rule) :- auxpass(Verbo, N1, Aux, N2), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N2, C2),  map(Verbo, Cost), R1 =..[auxpass, Cost, C2], R3=..[Aux, C2], Provv2=[R1, R3| Provv], assert(map(Aux, C2)), retract(auxpass(Verbo, N1, Aux, N2)), getRulesRic(Provv2, Rule).


%DOBJ
getRulesRic(Provv, Rule) :- dobj(Verbo, N1, Oggetto, N2),   name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N2, C2),  map(Verbo, Cost), R1 =..[dobj, Cost, C2], Provv2=[R1| Provv], assert(map(Oggetto, C2)), retract(dobj(Verbo, N1, Oggetto, N2)), getRulesRic(Provv2, Rule).
%getRulesRic(Provv, Rule) :- dobj(Verbo, N1, Oggetto, N2), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N2, C2),  map(Verbo, Cost), R1 =..[dobj, Cost, C2], R3=..[tax, C2, [Oggetto]], Provv2=[R1, R3| Provv], assert(map(Oggetto, C2)), retract(dobj(Verbo, N1, Oggetto, N2)), getRulesRic(Provv2, Rule).

%COP2
getRulesRic(Provv, Rule) :- cop2(Verbo, N1, Copula, N2), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N2, C2),  map(Verbo, Cost), R1 =..[dobj, Cost, C2], Provv2=[R1| Provv], assert(map(Copula, C2)), retract(cop2(Verbo, N1, Copula, N2)), getRulesRic(Provv2, Rule).

/*
%DET
getRulesRic(Provv, Rule) :- det(SoOgg, N1, Det, N2), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N2, C2),  map(SoOgg, Cost), R1 =..[det, Cost, C2], R3=..[Det, C2], Provv2=[R1, R3| Provv], assert(map(Det, C2)), retract(det(SoOgg, N1, Det, N2)), getRulesRic(Provv2, Rule).

%POSS
getRulesRic(Provv, Rule) :- poss(SoOgg, N1, Poss, N2), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N2, C2),  map(SoOgg, Cost), R1 =..[poss, Cost, C2], R3=..[Poss, C2], Provv2=[R1, R3| Provv], assert(map(Poss, C2)), retract(poss(SoOgg, N1, Poss, N2)), getRulesRic(Provv2, Rule).


%AMOD
getRulesRic(Provv, Rule) :- amod(SoOgg, N1, Amod, N2), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N2, C2),  map(SoOgg, Cost), R1 =..[amod, Cost, C2], R3=..[tax, C2, [Amod]], Provv2=[R1, R3| Provv], assert(map(Amod, C2)), retract(amod(SoOgg, N1, Amod, N2)), getRulesRic(Provv2, Rule).


%NUM
getRulesRic(Provv, Rule) :- num(SoOgg, N1, Num, N2), map(SoOgg, Cost), R1 =..[num, Cost, Num], Provv2=[R1 | Provv], retract(num(SoOgg, N1, Num, N2)), getRulesRic(Provv2, Rule).

%ADVMOD
getRulesRic(Provv, Rule) :- advmod(X, N1, Advmod, N2), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N2, C2),  map(X, Cost), R1 =..[advmod, Cost, C2], R3=..[tax, C2, [Advmod]], Provv2=[R1, R3| Provv], assert(map(Advmod, C2)), retract(advmod(X, N1, Advmod, N2)), getRulesRic(Provv2, Rule).


%PREP POBJ
getRulesRic(Provv, Rule) :- prep(X, N1, Prep, N2), pobj(Prep, N2, Pobj, N3), nnp(Pobj), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N2, C2), atom_concat(Z, N3, C3),  map(X, Cost), R1 =..[prep, Cost, C2], R2=..[pobj, C2, C3], R3=..[Prep, C2], R4=..[Pobj, C3], NNP=..[nnp, C3],  Provv2=[R1, R2, R3, R4| Provv], assert(map(Pobj, C3)), retract(prep(X, N1, Prep, N2)), retract(pobj(Prep, N2, Pobj, N3)), getRulesRic(Provv2, Rule).
getRulesRic(Provv, Rule) :- prep(X, N1, Prep, N2), pobj(Prep, N2, Pobj, N3), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N2, C2), atom_concat(Z, N3, C3),  map(X, Cost), R1 =..[prep, Cost, C2], R2=..[pobj, C2, C3], R3=..[Prep, C2], R4=..[tax, C3, [Pobj]],  Provv2=[R1, R2, R3, R4| Provv], assert(map(Pobj, C3)), retract(prep(X, N1, Prep, N2)), retract(pobj(Prep, N2, Pobj, N3)), getRulesRic(Provv2, Rule).


%AUX
getRulesRic(Provv, Rule) :- aux(Verbo, N1, Aux, N2), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N2, C2),  map(Verbo, Cost), R1 =..[aux, Cost, C2], R3=..[Aux, C2], Provv2=[R1, R3| Provv], assert(map(Aux, C2)), retract(aux(Verbo, N1, Aux, N2)), getRulesRic(Provv2, Rule).


%NEG
getRulesRic(Provv, Rule) :- neg(Verbo, N1, Neg, N2), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N2, C2),  map(Verbo, Cost), R1 =..[neg, Cost, C2], R3=..[Neg, C2], Provv2=[R1, R3| Provv], assert(map(Neg, C2)), retract(neg(Verbo, N1, Neg, N2)), getRulesRic(Provv2, Rule).


%CC CONJ
getRulesRic(Provv, Rule) :- cc(SoOgg, N1, Cong, N2), conj(SoOgg, N1, SoOgg2, N3), nnp(SoOgg2), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N2, C2), atom_concat(Z, N3, C3),  map(SoOgg, Cost), R1 =..[cc, Cost, C2], R2=..[conj, Cost, C3], R3=..[Cong, C2], R4=..[SoOgg2, C3], NNP=..[nnp, C3], Provv2=[R1, R2, R3, R4| Provv], assert(map(SoOgg2, C3)), retract(cc(SoOgg, N1, Cong, N2)), retract(conj(SoOgg, N1, SoOgg2, N3)), getRulesRic(Provv2, Rule).
getRulesRic(Provv, Rule) :- cc(SoOgg, N1, Cong, N2), conj(SoOgg, N1, SoOgg2, N3), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N2, C2), atom_concat(Z, N3, C3),  map(SoOgg, Cost), R1 =..[cc, Cost, C2], R2=..[conj, Cost, C3], R3=..[Cong, C2], R4=..[tax, C3, [SoOgg2]], Provv2=[R1, R2, R3, R4| Provv], assert(map(SoOgg2, C3)), retract(cc(SoOgg, N1, Cong, N2)), retract(conj(SoOgg, N1, SoOgg2, N3)), getRulesRic(Provv2, Rule).


%PRT
getRulesRic(Provv, Rule) :- prt(Verbo, N1, Prt, N2), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N2, C2),  map(Verbo, Cost), R1 =..[prt, Cost, C2], R3=..[Prt, C2], Provv2=[R1, R3| Provv], assert(map(Prt, C2)), retract(prt(Verbo, N1, Prt, N2)), getRulesRic(Provv2, Rule).

%NUMBER
getRulesRic(Provv, Rule) :- number(Num, N1, Number, N2), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N2, C2),  R1 =..[number, Num, C2], Provv2=[R1| Provv], retract(number(Num, N1, Number, N2)), getRulesRic(Provv2, Rule).


%QUANTMOD
getRulesRic(Provv, Rule) :- quantmod(X, N1, Quantmod, N2), name_cat(Ca), atom_concat(Ca, c, Z), atom_concat(Z, N2, C2),  R1 =..[quantmod, X, C2], R3=..[Quantmod, C2], Provv2=[R1, R3| Provv], assert(map(Quantmod, C2)), retract(quantmod(X, N1, Quantmod, N2)), getRulesRic(Provv2, Rule).

*/

getRulesRic(Provv, Provv).



/*
sport(f14) :- sentence(frase, soggetto), nsubj() subj(f14), massa(s(f14))

sport(f14) :- soggetto(f14, c2), verbo(f14, c1), oggetto(f14, c3), nsubj(c1, c2), john(c2), loves(c1), dobj(c1, c3), mary(c3), det(c3, c4), an(c4).

det(X, Y), nsubj(V, X), getRule(Y, X, R, det)
det(X, Y), dobj(V, X)
det(X, Y), nsubjpass(V, X)

getRule(Y, X, R, C) :- S=..[C, c3, c4], D=..[Y, c4], concat(S, D, R).
*/

got_atom(Term,Atom) :-
	nonvar(Atom), !,
	atom_codes(Atom,S),
	charsio:read_from_chars(S,Term).
got_atom(Term,Atom) :-
	charsio:write_to_chars(Term,S),
	atom_codes(Atom,S).
