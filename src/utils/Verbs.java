package utils;

import static java.util.Arrays.asList;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.annolab.tt4j.TokenHandler;
import org.annolab.tt4j.TreeTaggerException;
import org.annolab.tt4j.TreeTaggerWrapper;

import utils.models.BaseForm;
import utils.models.VerbType;
import utils.models.Word;

public class Verbs {

	// restituisce una lista vuota se non esistono passati, futuri ed ing form

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<BaseForm> getBaseForm(String[] sent,
			TreeTaggerWrapper tagger) throws IOException, TreeTaggerException {

		final List<BaseForm> baseForm = new LinkedList<>();
		final List<Word> words = new LinkedList<>();

		tagger.setHandler(new TokenHandler<String>() {

			public void token(String token, String pos, String lemma) {

				words.add(new Word(token, pos, lemma));

				switch (lemma) {
				case "be":
					BaseForm verb = new BaseForm(token, lemma, VerbType.BE);
					baseForm.add(verb);
					break;

				default:
					break;
				}

				System.out.println(token + "\t" + pos + "\t" + lemma);
			}

		});
		tagger.process(asList(sent));

		List<BaseForm> base = findPatterns(words);

		if (!base.isEmpty()) {
			baseForm.addAll(base);
		}
		return baseForm;
	}

	// aggiunge ed elimina predicati in caso di futuro passato o ing form
	public static List<String> getNormalizedRelations(List<BaseForm> baseForms,
			List<String> relations) {

		// CONTROLLO SULLE BASE FORM
		if (!baseForms.isEmpty()) {
			for (BaseForm b : baseForms) {
				// System.out.println(b.getBase_form());
				// ogni verbo particolare deve essere sostituito con la sua base
				// form
				for (int i = 0; i < relations.size(); i++) {
					if (relations.get(i).contains(", " + b.getVerb())
							|| relations.get(i).contains("(" + b.getVerb())) {

						relations.set(
								i,
								relations.get(i).replaceAll(b.getVerb(),
										b.getBase_form()));
					}
				}

				// deve essere aggiunto un nuovo type contenente il tipo di
				// verbo
				if (!b.getType().equals(VerbType.BE)
						&& !relations.contains("type(" + b.getBase_form()
								+ ", " + b.getType().toString().toLowerCase()
								+ ")")) {
					relations.add("type(" + b.getBase_form() + ", "
							+ b.getType().toString().toLowerCase() + ")");
				}
				// bisogna rimuovere tutti gli aux che contengono il verbo
				// corrente alla base form
				Iterator<String> iter = relations.iterator();

				while (iter.hasNext()) {
					String curr = iter.next();
					if (curr.contains("aux") && curr.contains(b.getBase_form())) {
						iter.remove();
					} else if (curr.contains("auxpass")
							&& curr.contains(b.getBase_form())) {

						iter.remove();

					}

				}

			}

		}

		return relations;
	}

	private static List<BaseForm> findPatterns(List<Word> words) {
		List<BaseForm> bases = new LinkedList<>();
		for (int i = 0; i < words.size(); i++) {
			BaseForm base = null;
			Word curr = words.get(i);
			String currPos = curr.getPos();

			if (i == 0) {
				switch (currPos) {
				case "VBD":

					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.PAST);
					break;

				case "VVD":

					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.PAST);
					break;
				case "VVZ":

					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.THIRD);
					break;
				case "VBZ":

					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.THIRD);
					break;
				case "VVN":

					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.PAST);

					break;

				case "VVG":

				default:
					break;
				}

				if (base != null)
					bases.add(base);
				continue;
			}

			Word prev = words.get(i - 1);
			String prevPos = prev.getPos();

			switch (currPos) {
			case "VBD":

				base = new BaseForm(curr.getToken(), curr.getLemma(),
						VerbType.PAST);
				break;
			case "VHN":
				if (prevPos.equals("VHZ")) {
					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.PASSIVE_PAST);
				} else if (i - 2 >= 0
						&& words.get(i - 2).getPos().equals("VHZ")) {
					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.PAST);
				} else if (i - 3 >= 0
						&& words.get(i - 3).getPos().equals("VHZ")) {
					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.PAST);
				}
				break;
			case "VVD":

				base = new BaseForm(curr.getToken(), curr.getLemma(),
						VerbType.PAST);
				break;
			case "VVZ":

				base = new BaseForm(curr.getToken(), curr.getLemma(),
						VerbType.THIRD);
				break;
			case "VBZ":

				base = new BaseForm(curr.getToken(), curr.getLemma(),
						VerbType.THIRD);
				break;
			case "VVN":

				if (prevPos.equals("VHP") || prevPos.equals("VHD")
						|| prevPos.equals("VHZ")) {
					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.PAST);

				} else if (prevPos.equals("VBN")
						&& i - 2 >= 0
						&& (words.get(i - 2).getPos().equals("VHZ") || words
								.get(i - 2).getPos().equals("VHP"))) {
					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.PASSIVE_PAST);
				} else if (prevPos.equals("VBD")) {
					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.PASSIVE_PAST);
				} else if (i - 2 >= 0
						&& (words.get(i - 2).getPos().equals("VBD")
								|| words.get(i - 2).getPos().equals("VBP") || words
								.get(i - 2).getPos().equals("VBZ"))) {
					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.PASSIVE_PAST);
				} else if (prevPos.equals("VBZ") || prevPos.equals("VBP")) {
					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.PASSIVE);
				} else if (prevPos.equals("VH")) {
					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.FUTURE);
				} else if (i - 2 >= 0
						&& (words.get(i - 2).getPos().equals("VHP")
								|| words.get(i - 2).getPos().equals("VHD") || words
								.get(i - 2).getPos().equals("VHZ"))) {

					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.PAST);

				} else if (i - 2 >= 0 && words.get(i - 2).getPos().equals("VH")) {
					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.FUTURE);

				} else {
					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.PAST);
				}

				break;

			case "VV":

				if (prevPos.equals("MD")) {

					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.FUTURE);
				}

				break;

			case "VVG":

				if (prevPos.equals("VBP") || prevPos.equals("VBZ")) {

					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.PRESENT_PROGRESSIVE);
				} else if (prevPos.equals("VB")) {

					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.FUTURE_PROGRESSIVE);
				} else if (prevPos.equals("VBD")) {

					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.PAST_PROGRESSIVE);
				} else if (i - 2 >= 0 && prevPos.equals("VBN")) {

					if (words.get(i - 2).getPos().equals("VHD")) {
						base = new BaseForm(curr.getToken(), curr.getLemma(),
								VerbType.PAST_PROGRESSIVE);
					} else if (words.get(i - 2).getPos().equals("VHP")
							|| words.get(i - 2).getPos().equals("VHZ")) {
						base = new BaseForm(curr.getToken(), curr.getLemma(),
								VerbType.PRESENT_PROGRESSIVE);
					} else if (words.get(i - 2).getPos().equals("VH")) {
						base = new BaseForm(curr.getToken(), curr.getLemma(),
								VerbType.FUTURE_PROGRESSIVE);
					}
				} else if (i - 2 >= 0
						&& (words.get(i - 2).getPos().equals("VBP") || words
								.get(i - 2).getPos().equals("VBZ"))) {
					base = new BaseForm(curr.getToken(), curr.getLemma(),
							VerbType.PRESENT_PROGRESSIVE);
				}

				break;

			default:
				break;
			}

			if (base != null)
				bases.add(base);

		}

		return bases;
	}
}
