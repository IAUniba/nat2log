
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.annolab.tt4j.TreeTaggerException;
import org.annolab.tt4j.TreeTaggerWrapper;

import utils.Utils;
import utils.Verbs;
import utils.models.BaseForm;

import com.declarativa.interprolog.PrologEngine;
import com.declarativa.interprolog.YAPSubprocessEngine;
import com.declarativa.interprolog.util.IPAbortedException;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;

public class SentenceConverter {

	GrammaticalStructureFactory gsf;
	TreeTaggerWrapper tagger;
	PrologEngine engine;
	LexicalizedParser lp;

	public SentenceConverter() throws IOException {

		// parser
		lp = LexicalizedParser
				.loadModel("edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz");
		TreebankLanguagePack tlp = new PennTreebankLanguagePack();
		gsf = tlp.grammaticalStructureFactory();

		// treetagger
		System.setProperty("treetagger.home", "res/treeTagger");
		tagger = new TreeTaggerWrapper<String>();
		tagger.setModel("english-utf8.par");

		// prolog
		engine = new YAPSubprocessEngine("/usr/bin/yap", false);
		File f = new File("res/prolog/main.pl");
		engine.consultAbsolute(f);

	}

	public List<String> getConversion(String path) throws IOException, TreeTaggerException {
		List<String> fin = new LinkedList<>();
		int i=0;
		int count = 0; 
		for (List<HasWord> sentence : new DocumentPreprocessor(
				path)) {
			System.out.println("FRASE "+i++);

			String[] sent = Utils.getSentence(sentence);

			List<BaseForm> baseForms = Verbs.getBaseForm(sent, tagger);

			Tree parse = lp.apply(sentence);
			// parse.pennPrint();
			// System.out.println();

			// ottiene le dipendenze dal parser
			GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
			Collection<TypedDependency> ti = gs.allTypedDependencies();

			List<String> relations = Utils.relationPreprocessing(ti);
			List<String> newRelations = Verbs.getNormalizedRelations(baseForms,
					relations);

			 for(String s:newRelations){
			 System.out.println(s);
			 }
			 System.out.println();

			for (String s : newRelations) {
				engine.deterministicGoal("assertz(" + s + ")");
			}

			

			Object[] bindings = engine.deterministicGoal("sentence(S)",
					"[string(S)]");
			if (bindings == null) {
				for (String s : newRelations) {
					engine.deterministicGoal("retract(" + s + ")");
				}
				count++;
				continue;
			}
			String message = (String) bindings[0];
			fin.add(message);

			// Retract
			for (String s : newRelations) {
				engine.deterministicGoal("retract(" + s + ")");
			}
			// System.out.println("\nSentence:" + message + "\n");

		}

		engine.shutdown();
		System.out.println("Non riconosciute: "+count);
		return fin;
		
		
	}

	

	

	

	

}
