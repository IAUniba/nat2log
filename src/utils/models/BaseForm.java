package utils.models;


public class BaseForm {
	
	
	private String verb;
	private String base_form;
	private VerbType type;

	
	
	
	public BaseForm(String verb, String base_form, VerbType type) {
		this.verb = verb;
		this.base_form = base_form;
		this.type = type;
	}
	public String getBase_form() {
		return base_form;
	}
	public VerbType getType() {
		return type;
	}
	public String getVerb() {
		return verb;
	}
	public void setBase_form(String base_form) {
		this.base_form = base_form;
	}
	public void setType(VerbType type) {
		this.type = type;
	}
	public void setVerb(String verb) {
		this.verb = verb;
	}
}
