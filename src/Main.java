import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import org.annolab.tt4j.TreeTaggerException;

public class Main {

	public static void main(String[] args) {

		boolean bool = true;
		

		while (bool) {
			
			try {
				boolean boolInt = true;
				boolean boolInt2 = true;
				System.out.println("Choose task: ");
				System.out.println("1. Sentence Conversion;");
				System.out.println("2. Inthelex Conversion;");
				System.out.println("0. Exit.");

				BufferedReader bufferRead = new BufferedReader(
						new InputStreamReader(System.in));
				String s = bufferRead.readLine();

				switch (s) {
				case "1":
					while (boolInt) {
						System.out.println("Make your choise:");
						System.out.println("1. Input file;");
						System.out.println("0. Back.");

						s = bufferRead.readLine();

						switch (s) {
						case "1":
							System.out.println("Write input file path: \n");
							bufferRead = new BufferedReader(
									new InputStreamReader(System.in));
							s = bufferRead.readLine();

							SentenceConverter conv = new SentenceConverter();
							List<String> message = conv.getConversion(s);

							
							for (String a : message) {
								System.out.println(a + " \n\n");
							}

							break;

						case "0":
							boolInt = false;
							break;

						default:
							System.out.println("Wrong choise. Retry. \n\n");
							break;
						}

					}

					break;

				case "2":
					while (boolInt2) {
						System.out.println("Make your choise:");
						System.out.println("1. Input directory;");
						System.out.println("0. Back.");

						s = bufferRead.readLine();

						switch (s) {
						case "1":
							System.out.println("Write directory path: \n");
							bufferRead = new BufferedReader(
									new InputStreamReader(System.in));
							s = bufferRead.readLine();

							InthelexPre pre = new InthelexPre();
							List<String> message = pre.preProcess(s);

							File file = new File("./res/nat2logNegTax.tun");
							if (!file.exists()) {
								file.createNewFile();
							}
							FileWriter fw = new FileWriter(file.getAbsoluteFile());
							BufferedWriter bw = new BufferedWriter(fw);
							for (String a : message) {
								
								if(a.equals("")) continue;
								bw.write(a+".\n");
								
								
								System.out.println(a + " \n\n");
							}
							bw.close();
							fw.close();

							break;

						
						case "0":
							boolInt2 = false;
							break;

						default:
							System.out.println("Wrong choise. Retry. \n\n");
							break;
						}

					}

					break;
				case "0":
					bool = false;
					break;

				default:
					System.out.println("Wrong choise. Retry. \n\n");
					break;
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TreeTaggerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		System.exit(0);

	}

}
