
%_______NORMALE
trova_soggetti(Sogg, Sogg1) :- cc(Sogg1, Cong), nsubj(Ve, Sogg1), conj(Sogg1, Sogg2), L=[Sogg1, Sogg2], trova_altri(Sogg2, Lista), list_concat([L, Lista], S), controlla_altro(S, Soggetto), get_string(Cong, Soggetto, Sogg),!.

trova_soggetti(Sogg, Sogg1) :- cc(Sogg1, Cong), nsubj(Ve, Sogg1), conj(Sogg1, Sogg2), L=[Sogg1, Sogg2], trova_altri(Sogg2, Lista), list_concat([L, Lista], Soggetto), get_string(Cong, Soggetto, Sogg), !.

trova_soggetti(Sogg, Sogg1) :- cc(Sogg1, Cong), nsubj(Ve, Sogg1), conj(Sogg1, Sogg2), S=[Sogg1, Sogg2], controlla_altro(S, Soggetto), get_string(Cong, Soggetto, Sogg), !.

trova_soggetti(Sogg, Sogg1) :- cc(Sogg1, Cong), nsubj(Ve, Sogg1), conj(Sogg1, Sogg2), Sogg=..[Cong, Sogg1, Sogg2], !.

%________PASSIVO
trova_soggetti(Sogg, Sogg1) :- cc(Sogg1, Cong), nsubjpass(Ve, Sogg1), conj(Sogg1, Sogg2), L=[Sogg1, Sogg2], trova_altri(Sogg2, Lista), list_concat([L, Lista], S), controlla_altro(S, Soggetto), get_string(Cong, Soggetto, Sogg),!.

trova_soggetti(Sogg, Sogg1) :- cc(Sogg1, Cong), nsubjpass(Ve, Sogg1), conj(Sogg1, Sogg2), L=[Sogg1, Sogg2], trova_altri(Sogg2, Lista), list_concat([L, Lista], Soggetto), get_string(Cong, Soggetto, Sogg), !.

trova_soggetti(Sogg, Sogg1) :- cc(Sogg1, Cong), nsubjpass(Ve, Sogg1), conj(Sogg1, Sogg2), S=[Sogg1, Sogg2], controlla_altro(S, Soggetto), get_string(Cong, Soggetto, Sogg), !.

trova_soggetti(Sogg, Sogg1) :- cc(Sogg1, Cong), nsubjpass(Ve, Sogg1), conj(Sogg1, Sogg2), Sogg=..[Cong, Sogg1, Sogg2], !.

%______________

trova_soggetti(Sogg, Sogg1) :- nsubjpass(Ve, Sogg1), S=[Sogg1], controlla_altro(S, Soggetto), get_string(Cong, Soggetto, Sogg),! .

trova_soggetti(Sogg, Sogg1) :- nsubj(Ve, Sogg1), S=[Sogg1], controlla_altro(S, Soggetto), get_string(Cong, Soggetto, Sogg), !.

trova_soggetti(Sogg1, Sogg1).



%________________CONTROLLARE IL PREDICATO NOMINALE cop2

trova_pnom(Ogg, Ogg1) :- cc(Ogg1, Cong), cop2(Ogg1, Cop), conj(Ogg1, Ogg2), L=[Ogg1, Ogg2], trova_altri(Ogg2, Lista), list_concat([L, Lista], S), controlla_altro(S, Oggetto), get_string(Cong, Oggetto, Ogg),!.

trova_pnom(Ogg, Ogg1) :- cc(Ogg1, Cong), cop2(Ogg1, Cop), conj(Ogg1, Ogg2), L=[Ogg1, Ogg2], trova_altri(Ogg2, Lista), list_concat([L, Lista], Oggetto), get_string(Cong, Oggetto, Ogg), !.

trova_pnom(Ogg, Ogg1) :- cc(Ogg1, Cong), cop2(Ogg1, Cop), conj(Ogg1, Ogg2), S=[Ogg1, Ogg2], controlla_altro(S, Oggetto), get_string(Cong, Oggetto, Ogg), !.

trova_pnom(Ogg, Ogg1) :- cc(Ogg1, Cong), cop2(Ogg1, Cop), conj(Ogg1, Ogg2), Ogg=..[Cong, Ogg1, Ogg2], !.

trova_pnom(Ogg, Ogg1) :- cop2(Ogg1, Cop),  S=[Ogg1], controlla_altro(S, Oggetto), get_string(Cong, Oggetto, Ogg), !.

trova_pnom(Ogg, Ogg1) :- cop2(Ogg1, Cop),  Ogg=[Ogg1], !.


















%_______________________________

get_string(Cong, [H|T], H) :- var(Cong), scandisci_lista([H|T], String, Provv),  !.
get_string(Cong, Sogg, Str) :- scandisci_lista(Sogg, String, Provv), atom_concat(Cong, '(', Str2), atom_concat(Str2, String, Str3), atom_concat(Str3, ')', Str4), got_atom(Str, Str4), !.
scandisci_lista([], Provv, Provv).
scandisci_lista([H|[]], Str, Provv) :- var(Provv), got_atom(H, Str).
scandisci_lista([H|[]], Str, Provv) :- got_atom(H, A), atom_concat(Provv, A, Str).
scandisci_lista([H|T], Str, Provv) :- var(Provv), got_atom(H, A), atom_concat(A, ',', L), scandisci_lista(T, Str, L).
scandisci_lista([H|T], Str, Provv) :- got_atom(H, A), atom_concat(Provv, A, L), atom_concat(L, ',', L1), scandisci_lista(T, Str, L1).



controlla_altro([H|T], Sogg) :- controlla([H|T], Sogg, Provv), !.
controlla([], Provv, Provv).
controlla([H|T], Sogg, Provv) :- Provv\=[], trova_relazioni(H, Rel),  list_concat([[Rel],Provv], S), controlla(T, Sogg, S).
controlla([H|T], Sogg, Provv) :- trova_relazioni(H, Rel), S=[Rel], controlla(T, Sogg, S).
controlla([H|T], Sogg, Provv) :- Provv\=[], list_concat([[H],Provv], S), controlla(T, Sogg, S).
controlla([H|T], Sogg, Provv) :- S=[H], controlla(T, Sogg, S).


trova_altri(Sogg, L) :- trova_rec(Sogg, L, Provv), !.
trova_rec(Sogg2, L, Provv) :- cc(Sogg2, Cong), conj(Sogg2, Sogg3), Provv\=[], list_concat([[Sogg3],Provv], S), trova_rec(Sogg3, L, S).
trova_rec(Sogg2, L, Provv) :- cc(Sogg2, Cong), conj(Sogg2, Sogg3), S=[Sogg3], trova_rec(Sogg3, L, S).
trova_rec(Sogg2, L, Provv) :- cc(Sogg2, Cong), conj(Sogg2, Sogg3), Provv\=[], list_concat([[Sogg3], Provv], L).
trova_rec(Sogg2, L, Provv) :- cc(Sogg2, Cong), conj(Sogg2, Sogg3), L=[Sogg3].




verbo_altro(V, List) :-  altroRic(V, List, [], [], Retr),  asserisci(Retr), !.

altroRic(V, List, Provv, R, Retr) :- type(V, Type), retract(type(V, Type)), altroRic(V, List, [Type|Provv], [type(V, Type)|R], Retr).
altroRic(V, List, Provv, R, Retr) :- acomp(V, Mod),  retract(acomp(V, Mod)), altroRic(V, List, [Mod|Provv], [acomp(V, Mod)|R], Retr).
altroRic(V, List, Provv, R, Retr) :- neg(V, Neg), retract(neg(V, Neg)), altroRic(V, List, [Neg|Provv], [neg(V, Neg)|R], Retr).
altroRic(V, List, Provv, R, Retr) :- auxpass(V, Aux), retract(auxpass(V, Aux)), altroRic(V, List, [Aux|Provv], [auxpass(V, Aux)|R], Retr).
altroRic(V, List, Provv, R, Retr) :- cop2(J, V), aux(J, Aux), retract(aux(J, Aux)), altroRic(V, List, [Aux|Provv], [aux(J, Aux)|R], Retr).
altroRic(V, List, Provv, R, Retr) :- aux(V, Aux), retract(aux(V, Aux)), altroRic(V, List, [Aux|Provv], [aux(V, Aux)|R], Retr).
altroRic(V, List, Provv, R, Retr) :- mark(V, Type), retract(mark(V, Type)), altroRic(V, List, [Type|Provv], [mark(V, Type)|R], Retr).
altroRic(V, List, Provv, R, Retr) :- advmod(V, Mod), retract(advmod(V, Mod)), altroRic(V, List, [Mod|Provv], [advmod(V, Mod)|R], Retr).
altroRic(V, List, Provv, R, Retr) :- expl(V, Exp), retract(expl(V, Exp)), altroRic(V, List, [Exp|Provv], [expl(V, Exp)|R], Retr).


altroRic(V, List, Provv, R, R) :- prt(V, Ph), atom_concat(V, '_', Ve1), atom_concat(Ve1, Ph, V1),  List=[V1|Provv].
altroRic(V, [V|Provv], Provv, R, R).


asserisci([]).
asserisci([H|T]) :- assert(H), asserisci(T).




trova_relazioni(A, S) :- relazioni(A, [H|T]), costruisci_relazione(H, T, S), !.
relazioni(A, List) :- relazioniRic(A, List, [], [], Retr), asserisci(Retr).



relazioniRic(A, List, Provv, R, Retr) :- prep(A, Prep), pobj(Prep, Pobj), retractall(prep(A, Prep)), retractall(pobj(Prep, Pobj)), find_OtherRel(Pobj, Rel, [Prep|Provv]),  relazioniRic(A, List, [Pobj|Rel], [prep(A, Prep), pobj(Prep, Pobj)|R], Retr).
relazioniRic(A, List, Provv, R, Retr) :- predet(A, Pred), retractall(predet(A, Pred)), relazioniRic(A, List, [Pred|Provv], [predet(A, Pred)|R], Retr).
relazioniRic(A, List, Provv, R, Retr) :- poss(A, Poss), retractall(poss(A, Poss)), relazioniRic(A, List, [Poss|Provv], [poss(A, Poss)|R], Retr).
relazioniRic(A, List, Provv, R, Retr) :- quantmod(A, Quantmod), retractall(quantmod(A, Quantmod)), relazioniRic(A, List, [Quantmod|Provv], [quantmod(A, Quantmod)|R], Retr).
relazioniRic(A, List, Provv, R, Retr) :- number(A, Number), retractall(number(A, Number)), find_OtherRel(Number, Rel, Provv),  L=[Number|Rel], relazioniRic(A, List, L, [number(A, Number)|R], Retr).
relazioniRic(A, List, Provv, R, Retr) :- num(A, Num), retractall(num(A, Num)), find_OtherRel(Num, Rel, Provv),  L=[Num|Rel],  relazioniRic(A, List, L, [num(A, Num)|R], Retr).
relazioniRic(A, List, Provv, R, Retr) :- det(A, Det), retractall(det(A, Det)), relazioniRic(A, List, [Det|Provv], [det(A, Det)|R], Retr).
relazioniRic(A, List, Provv, R, Retr) :- amod(A, Amod), retractall(amod(A, Amod)), relazioniRic(A, List, [Amod|Provv], [amod(A, Amod)|R], Retr).
relazioniRic(A, List, Provv, R, Retr) :- advmod(A, Advmod), retractall(advmod(A, Advmod)), relazioniRic(A, List, [Advmod|Provv], [advmod(A, Advmod)|R], Retr).
relazioniRic(A, List, Provv, R, Retr) :- neg(A, Neg), retractall(neg(A, Neg)), relazioniRic(A, List, [Neg|Provv], [neg(A, Neg)|R], Retr).
relazioniRic(A, List, Provv, R, Retr) :- nn2(A, Nn), retractall(nn2(A, Nn)), relazioniRic(A, List, [Nn|Provv], [nn2(A, Nn)|R], Retr).
%relazioniRic(A, List, Provv, R, Retr) :- aux(A, Aux), retractall(aux(A, Aux)), relazioniRic(A, List, [Aux|Provv], [aux(A, Aux)|R], Retr).

relazioniRic(A, [A|Provv], Provv, R, R).


find_OtherRel(Pobj, Rel, Provv) :- find_ric(Pobj, Rel, Provv, [], Retr), asserisci(Retr), !.

find_ric(A, List, Provv, R, Retr) :- predet(A, Pred), retractall(predet(A, Pred)), find_ric(A, List, [Pred|Provv], [predet(A, Pred)|R], Retr).
find_ric(A, List, Provv, R, Retr) :- conj(A, Conj), cc(A, And), retractall(conj(A, Conj)), find_OtherRel(Conj, Rel, Provv), find_ric(A, List, [And, Conj|Rel], [conj(A, Conj)|R], Retr).
find_ric(A, List, Provv, R, Retr) :- poss(A, Poss), retractall(poss(A, Poss)), find_ric(A, List, [Poss|Provv], [poss(A, Poss)|R], Retr).
find_ric(A, List, Provv, R, Retr) :- quantmod(A, Quantmod), retractall(quantmod(A, Quantmod)), find_ric(A, List, [Quantmod|Provv], [quantmod(A, Quantmod)|R], Retr).
find_ric(A, List, Provv, R, Retr) :- num(A, Num), retractall(num(A, Num)), find_OtherRel(Num, Rel, Provv),  L=[Num|Rel],  find_ric(A, List, L, [num(A, Num)|R], Retr).
find_ric(A, List, Provv, R, Retr) :- number(A, Number), retractall(number(A, Number)), find_OtherRel(Number, Rel, Provv),  L=[Number|Rel],  find_ric(A, List, L, [number(A, Number)|R], Retr).
find_ric(A, List, Provv, R, Retr) :- det(A, Det), retractall(det(A, Det)), find_ric(A, List, [Det|Provv], [det(A, Det)|R], Retr).
find_ric(A, List, Provv, R, Retr) :- advmod(A, Advmod), retractall(advmod(A, Advmod)), find_ric(A, List, [Advmod|Provv], [advmod(A, Advmod)|R], Retr).
find_ric(A, List, Provv, R, Retr) :- neg(A, Neg), retractall(neg(A, Neg)), find_ric(A, List, [Neg|Provv], [neg(A, Neg)|R], Retr).
find_ric(A, List, Provv, R, Retr) :- amod(A, Amod), retractall(amod(A, Amod)), find_ric(A, List, [Amod|Provv], [amod(A, Amod)|R], Retr).
find_ric(A, List, Provv, R, Retr) :- nn2(A, Nn), retractall(nn2(A, Nn)), find_ric(A, List, [Nn|Provv], [nn2(A, Nn)|R], Retr).
%find_ric(A, List, Provv, R, Retr) :- aux(A, Aux), retractall(aux(A, Aux)), find_ric(A, List, [Aux|Provv], [aux(A, Aux)|R], Retr).
find_ric(A, List, Provv, R, Retr) :- prep(A, Prep), pobj(Prep, Pobj), retractall(prep(A, Prep)), retractall(pobj(Prep, Pobj)), find_OtherRel(Pobj, Rel, [Prep|Provv]), find_ric(A, List, [Pobj|Rel], [prep(A, Prep), pobj(Prep, Pobj)|R], Retr).

find_ric(A, Provv, Provv, R, R).


trova_complementi(Verbo, Lista) :- trova_complementiRic(Verbo, Lista, [], [], Retr), asserisci(Retr), !.
trova_complementiRic(Verbo, Lista, Provv, R, Retr) :- prep(Verbo, Prep), pobj(Prep, Pobj), retract(prep(Verbo, Prep)), retract(pobj(Prep, Pobj)), trova_relazioni(Pobj, Compl), Complemento=..[Prep, Compl],  trova_complementiRic(Verbo, Lista, [Complemento| Provv], [pobj(Prep, Pobj), prep(Verbo, Prep)|R], Retr).
%trova_complementiRic(Verbo, Lista, Provv, R, Retr) :- prep(Verbo, Prep), pobj(Prep, Pobj), Complemento=..[Prep, Pobj], retract(prep(Verbo, Prep)), retract(pobj(Prep, Pobj)), trova_complementiRic(Verbo, Lista, [Complemento| Provv], [prep(Verbo, Prep), pobj(Prep, Pobj)|R], Retr).
trova_complementiRic(Verbo, Lista, Provv, R, Retr) :- dobj(Verbo, Oggetto), retract(dobj(Verbo, Oggetto)), trova_relazioni(Oggetto, Compl),  trova_complementiRic(Verbo, Lista, [Compl| Provv], [dobj(Verbo, Oggetto)|R], Retr).
trova_complementiRic(Verbo, Provv, Provv, R, R).




%COSTRUISCE I CASI PARTICOLARI DI FRASI RELATIVE CON VARIABILI
costruisci_frase1(V, Sogg, V1, Ogg1, S) :- A=.. [V, Sogg, Variabile], Y=.. [V1, Variabile, Ogg1],  S=..[frase, A, Y].
costruisci_frase2(V, Ogg, V1, Ogg1, S):- A=.. [V, Variabile, Ogg], Y=.. [V1, Variabile, Ogg1],  S=..[frase, A, Y].
costruisci_frase3(V, Sogg, Sogg1, S):- A=.. [V, Sogg, Variabile], Y=.. [V1, Sogg1, Variabile],  S=..[frase, A, Y].


get_element([T], T).
%deve restituire l'elemento finale della lista.
get_verbo([T], [T]).
get_verbo([H|T], A) :- get_verbo([T|R], A).



costruisci_verbo(A, [], A).
costruisci2(A, [], Provv, Provv).
costruisci2(A, [R|T], Provv, S) :- P=..[R, Provv], costruisci2(A, T, P, S).
costruisci_verbo(A, [R|T], S) :- costruisci2(A, [R|T], A, S).

costruisci_relazione(A, [], A).
costruisci3(A, [], Provv, Provv).
costruisci3(A, [R|T], Provv, S) :- P=..[R, Provv], costruisci3(A, T, P, S).
costruisci_relazione(A, [R|T], S) :- costruisci3(A, [R|T], A, S).






get_concat([], Stringa).
get_concat(Lista, Stringa) :- get_concatRic(Lista, Stringa, Provv).
get_concatRic([H|[]], Stringa, Provv) :- var(Provv), Stringa = H.
get_concatRic([H|[]], Stringa, Provv) :- got_atom(H, Atom),  atom_concat(Provv, Atom, Stringa).
get_concatRic([H|T], Stringa, Provv) :- var(Provv),  got_atom(H, Atom), atom_concat(Atom, ', ', Str), get_concatRic(T, Stringa, Str).
get_concatRic([H|T], Stringa, Provv) :- got_atom(H, Atom), atom_concat(Provv, Atom, Str1), atom_concat(Str1, ',', Str),  get_concatRic(T, Stringa, Str).






got_atom(Term,Atom) :-
	nonvar(Atom), !,
	atom_codes(Atom,S),
	charsio:read_from_chars(S,Term).
got_atom(Term,Atom) :-
	charsio:write_to_chars(Term,S),
	atom_codes(Atom,S).
	

