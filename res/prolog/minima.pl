% reconsult('/Users/mac/Dropbox/maddy vito/prolog/daPazzi/minima.pl').

%:- reconsult('/home/tilly/git/iajavaprolog/res/prolog/commons.pl').
%:- reconsult('/home/madeleinecordasco/repos/IAJavaProlog/res/prolog/commons.pl').
%:- reconsult('/Users/mac/Dropbox/maddy vito/prolog/daPazzi/commons.pl').
:- reconsult('commons.pl').

:- use_module(library(lists)).
:- use_module(library(charsio)).

%per frase minima si intende la frase con solo soggetto e verbo, oppure una frase con copula tipo the book is red.

%caso in cui ci sia altro sia per soggetto che per verbo (o anche predicato nominale).
fraseMinima(S, SoggettoFin, Copula, PnomFin) :- fraseMinima1(S1, Soggetto, Copula, Pnom), trova_soggetti(SoggettoFin, Soggetto), verbo_altro(Copula, [V|T]), trova_pnom(PnomFin, Pnom), A=..[V, SoggettoFin, PnomFin], costruisci_verbo(A, T, S), !.
fraseMinima(S, SoggettoFin, Copula, PnomFin) :- fraseMinima1(S1, Soggetto, Copula, Pnom), trova_soggetti(SoggettoFin, Soggetto), trova_pnom(PnomFin, Pnom), A=..[Copula, SoggettoFin, PnomFin],  !.
fraseMinima(S, Soggetto, Copula, PnomFin) :- fraseMinima1(S1, Soggetto, Copula, Pnom), verbo_altro(Copula, [V|T]), trova_pnom(PnomFin, Pnom), A=..[V, Soggetto, PnomFin], costruisci_verbo(A, T, S), !.
fraseMinima(S, Soggetto, Copula, PnomFin) :- fraseMinima1(S1, Soggetto, Copula, Pnom), trova_pnom(PnomFin, Pnom), A=..[Copula, Soggetto, PnomFin],  !.
fraseMinima(S, SoggettoFin, Copula, Pnom) :- fraseMinima1(S1, Soggetto, Copula, Pnom), trova_soggetti(SoggettoFin, Soggetto), verbo_altro(Copula, [V|T]), A=..[V, SoggettoFin, Pnom], costruisci_verbo(A, T, S), !.
fraseMinima(S, SoggettoFin, Copula, Pnom) :- fraseMinima1(S1, Soggetto, Copula, Pnom), trova_soggetti(SoggettoFin, Soggetto), A=..[Copula, SoggettoFin, Pnom],  !.
fraseMinima(S, Soggetto, Copula, Pnom) :- fraseMinima1(S1, Soggetto, Copula, Pnom), verbo_altro(Copula, [V|T]), A=..[V, Soggetto, Pnom], costruisci_verbo(A, T, S), !.
fraseMinima(S, Soggetto, Copula, Pnom) :- fraseMinima1(S, Soggetto, Copula, Pnom), S=..[Copula, Soggetto, Pnom], !.


fraseMinima(S, SoggettoFin, Verbo) :- fraseMinima2(S1, Soggetto, Verbo), trova_soggetti(SoggettoFin, Soggetto), verbo_altro(Verbo, [V|T]), A=..[V, SoggettoFin], costruisci_verbo(A, T, S), !.
fraseMinima(S, Soggetto, Verbo) :- fraseMinima2(S1, Soggetto, Verbo), verbo_altro(Verbo, [V|T]), A=..[V, Soggetto], costruisci_verbo(A, T, S), !.
fraseMinima(S, SoggettoFin, Verbo) :- fraseMinima2(S1, Soggetto, Verbo), trova_soggetti(SoggettoFin, Soggetto), S=..[Verbo, SoggettoFin], !.
fraseMinima(S, Soggetto, Verbo) :- fraseMinima2(S, Soggetto, Verbo), S=..[Verbo, Soggetto], !.
%fraseMinima(S, Soggetto, Verbo) :- soggetto_verbo(Verbo, Sogg), trova_soggetti(Soggetto, Sogg), 
%fraseMinima(S, Soggetto, Verbo) :- soggetto_verbo(Verbo, Soggetto), verbo_altro(Verbo, [V|T]), A=..[Verbo, Soggetto],  costruisci_verbo(A, T, S), !.



%caso base 
fraseMinima1(S, Soggetto, Copula, Pnom) :- soggetto_verbo(Pnom, Soggetto), predNom_verbo(Pnom, Copula), S=..[Copula, Soggetto, Pnom], !.
fraseMinima2(S, Soggetto, Verbo) :- soggetto_verbo(Verbo, Soggetto), not(cop2(Verbo, Copula)), S=..[Verbo, Soggetto], !.




soggetto_verbo(Verbo, Soggetto) :- nsubj(Verbo, Soggetto), !.
soggetto_verbo(Verbo, Soggetto) :- nsubjpass(Verbo, Soggetto), !.
%soggetto_verbo(Verbo, Soggetto) :- nsubj2(Verbo, Soggetto), !.
%soggetto_verbo(Verbo, Soggetto) :- rcmod(Soggetto, Verbo), !.

%in questo caso nn c'è --> oggetto_verbo(Verbo, Oggetto) :- dobj(Verbo, Oggetto).
predNom_verbo(PredNominale, Copula) :- cop2(PredNominale, Copula).
